import React, { useState, useEffect } from 'react';
import axios from 'axios';
import DefaultConfig from '../../../Utils/Config';
import styled from 'styled-components';
import Variants from './Variants';
import Configuration from './Configuration';
import Transmission from './Transmission';
import Colors from './Colors';
import Accessory from './Accessory';
import PriceCalculation from './PriceCalculation';
import Summary from './Summary';
import AfterSales from './AfterSales';
import { useSelectedProducts } from '../../../Utils/Global';

const MultiStepTabs = ({ product, activeStepTab, handleStepTab, handleSelectedVariant, selectedVariant, selectedConfig, handleSelectConfig, selectedTransmission, handleSelectTransmission, handleSelectColor, selectedColor, selectedPackages, handleSelectPackage, selectedCustoms, handleSelectCustom, CookieDealer, formData, setFormData, handleSelectAfterSales, hasConfiguration, selectedTCare }) => {
    // const [activeTab, setActiveTab] = useState(1);

    const {
        getProductById,
        addProduct,
        removeProduct,
        updateProduct,
    } = useSelectedProducts();

    const [proceedToSummary, setProceedToSummary] = useState(false);
    const [triggerFormVerification, setTriggerFormVerification] = useState(false);

    const CookieProduct = getProductById(product?.id);

    const currentTimestamp = new Date().toISOString();

    const tabs = ['Variants', 'Configuration', 'Transmission', 'Colors', 'Accessories', 'AfterSales','Price Calculation', 'Summary'];
    // const handleTabClick = (tabNumber) => {
    //     setActiveTab(tabNumber);
    // };

    const handleNext = () => {
        if(
            activeStepTab===8 &&
            (
                !formData.budgetSimulation ||
                !formData.formName ||
                !formData.formPhone ||
                !formData.formAddress ||
                !formData.formEmail
            )
            ){
            setTriggerFormVerification(true);
        }else{
            if (!CookieProduct || CookieProduct.length === 0) {
                addProduct({ id: product.id, selectedVariant: selectedVariant.id, lastUpdated: currentTimestamp, completedTab: activeStepTab, nextTab: activeStepTab + 1 });
            } else {
                let additionalData = {
                    selectedVariant: selectedVariant.id,
                    lastUpdated: currentTimestamp
                };
                let oldData = CookieProduct;
                Object.assign(oldData, additionalData);
                updateProduct(product.id, oldData);
            }
            if (activeStepTab < tabs?.length) {
                if(!hasConfiguration && activeStepTab===1){
                    handleStepTab(activeStepTab + 2);
                }else{
                    handleStepTab(activeStepTab + 1);
                }
            }
        }
    };

    const handlePrev = () => {
        if (activeStepTab > 1) {
            if(!hasConfiguration && activeStepTab===3){
                handleStepTab(activeStepTab - 2);
            }else{
                handleStepTab(activeStepTab - 1);
            }
        }
    };




    const renderTabContent = () => {
        switch (activeStepTab) {
            case 1:
                return <Variants product={product} handleSelectedVariant={handleSelectedVariant} selectedVariant={selectedVariant} CookieDealer={CookieDealer} />;
            case 2:
                return <Configuration product={product} selectedVariant={selectedVariant} selectedConfig={selectedConfig} handleSelectConfig={handleSelectConfig} />;
            case 3:
                return <Transmission product={product} selectedVariant={selectedVariant} selectedTransmission={selectedTransmission} handleSelectTransmission={handleSelectTransmission} />;
            case 4:
                return <Colors handleSelectColor={handleSelectColor} selectedVariant={selectedVariant} selectedColor={selectedColor} product={product} />;
            case 5:
                return <Accessory selectedVariant={selectedVariant} selectedColor={selectedColor} product={product} selectedPackages={selectedPackages} handleSelectPackage={handleSelectPackage} selectedCustoms={selectedCustoms} handleSelectCustom={handleSelectCustom} />;
            case 6:
                return <AfterSales handleSelectAfterSales={handleSelectAfterSales} product={product} selectedVariant={selectedVariant} selectedTCare={selectedTCare}/>
            case 7:
                return <PriceCalculation product={product} selectedVariant={selectedVariant} CookieDealer={CookieDealer} selectedConfig={selectedConfig} selectedTransmission={selectedTransmission} selectedColor={selectedColor} selectedPackages={selectedPackages} selectedCustoms={selectedCustoms} activeStepTab={activeStepTab} triggerFormVerification={triggerFormVerification} formData={formData} setFormData={setFormData} />;
            case 8:
                return <Summary activeStepTab={activeStepTab} />;
            default:
                return null;
        }
    };

    return (
        <ProductTab>
            <div className='multistep-sec'>
                <div className="product-tabs">
                    {tabs?.map((tabText, index) => (
                        tabText!=="Configuration" || (tabText==='Configuration' && hasConfiguration)
                            ?<div
                                key={index + 1}
                                className={`tab ${activeStepTab === index + 1 ? 'active' : ''} ${activeStepTab > index + 1 ? 'visited' : ''
                                } ${ (activeStepTab === 1 && index===2) || activeStepTab === index ? 'next' : ''}`}
                            >
                                {tabText}
                            </div>
                            : null
                        
                    ))}
                </div>
                <div className="product-tab-content">{renderTabContent()}</div>
                {activeStepTab !== 8
                    ? (
                        <div className="buttons">
                            <button className='prev' onClick={handlePrev} disabled={activeStepTab === 1}>
                                Previous Step
                            </button>
                            <button
                                className='next'
                                onClick={handleNext}
                                disabled={(activeStepTab === 1 && selectedVariant.length === 0) || (activeStepTab === 4 && selectedColor.length === 0)}
                            >
                                Next Step
                            </button>

                        </div>
                    )
                    : null
                }
            </div>
        </ProductTab>
    );
};



const ProductTab = styled.div`
.multistep-sec{
    padding-bottom:60px;
    .product-tabs {
        display: flex;
        justify-content: center;
        overflow-x:hidden;
        white-space:nowrap;
        @media screen and (max-width:991px){
            justify-content:flex-start;
        }
        .tab {
            border-radius: 5px;
            background: #D9DADB;
            padding: 16px 32px;
            margin: 0 5px 10px;
            color: #FFF;
            text-align: center;
            font-weight: 700;
            line-height: 24px; 
            text-transform:uppercase;
            border: 2px solid #D9DADB;
            @media screen and (max-width:850px){
                 display:none;
            }
            &.active,&.next,&.visited{
                @media screen and (max-width:850px){
                    display:block;
                    flex:1;
               }
               @media screen and (max-width:850px){
                display:block;  
           }
            }
            &.visited{
                @media screen and (max-width:740px){
                    display:none;
               }
            }
            @media screen and (max-width:1199px){
                padding:10px 5px;
                font-size:14px;
            }
            &:last-child{
                background:#D71921;
                border: 2px solid #D71921;
            }
            &.active{
                background:#fff;
                border: 2px solid #D71921;
                color: #161A1D;
            }
            &.visited{
                background:#161A1D;
                border: 2px solid #161A1D;
                color: #fff;
            }
        }
    }
    .buttons{
        display:flex;
        justify-content:center;
        button{
            border-radius: 4px;
            padding: 14px 32px;
            margin:0 5px;
            color: #FFF;
            text-align: center;
            font-size: 16px;
            font-weight: 400;
            line-height: 20px;
            border:0;
        }
        .prev{
            background:#fff;
            color:#161A1D;
            transition:0.5s;
            &:hover{
                background: #F7D1D3;
                color: #D71921;
            }
        }
        .next{
            background:#161A1D;
            color:#fff;
            transition:0.5s;
            &:hover{
                background: #D71921;
            }
        }
    }
    .product-tab-content {
       
    }
}

`
export default MultiStepTabs;
