import React from 'react';
import { Header } from '../Header';
import { Footer } from '../Footer';
import CookiePopup from '../CookiePopup';


const Layout = ({ children }) => {
  return (
    <div>
      <Header />
      {children}
      <CookiePopup/>
      <Footer />
    </div>
  );
};

export default Layout;